package com.maven.hul;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Update_distributor {

	public static void main(String[] args) throws InterruptedException {
		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#");
		Thread.sleep(3000);
	 
		
	    //Enter Login details 
		driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("opsadmin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
		Thread.sleep(3000);
		driver.findElement(By.linkText("SIGN IN")).click();
		Thread.sleep(4000);
		//Click on Capacity tab
		driver.findElement(By.xpath("//span[text()='Capacity']/parent::span")).click();
		Thread.sleep(2000);
	
		// Enter code 
		driver.findElement(By.xpath("(//input[@name='name'])[1]")).sendKeys("134117");
		Thread.sleep(2000);
		
		//Click on search button
		driver.findElement(By.xpath("//span[text()='Search']")).click();
		Thread.sleep(2000);

		        //Click on edit button 
				WebElement element = driver.findElement(By.xpath("(//span[text()='Actions']/parent::span/parent::span/parent::a)[2]"));
				Actions actions = new Actions(driver);
				actions.moveToElement(element).click().perform();
				element.click();
				Thread.sleep(2000);
				
				//Click on employee edit button 
				driver.findElement(By.xpath("(//span[text()='Edit Details'])[2]/parent::a/parent::div")).click();
				Thread.sleep(3000);
		
				//Enter first name 
				driver.findElement(By.xpath("//input[@name='firstName']")).clear();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("Devkrupa Agencies Update ");
				Thread.sleep(1000);
				
				//Enter last name
				driver.findElement(By.xpath("//input[@name='lastName']")).clear();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("Update last name");
				Thread.sleep(1000);
				
				//Enter mobile
				driver.findElement(By.xpath("//input[@name='mobileNumber']")).clear();
				driver.findElement(By.xpath("//input[@name='mobileNumber']")).sendKeys("33445566");
				Thread.sleep(1000);
				
				//Enter Email
				driver.findElement(By.xpath("//input[@name='emailAddress']")).clear();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@name='emailAddress']")).sendKeys("testing@testing.com");
				Thread.sleep(1000);
                //Enter address
				driver.findElement(By.xpath("//input[@name='address']")).clear();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@name='address']")).sendKeys("Address update");
				Thread.sleep(1000);
				
				
				//Click on update button
				driver.findElement(By.xpath("//span[text()='Update']")).click();
				Thread.sleep(15000);
			
				driver.quit();
}
}