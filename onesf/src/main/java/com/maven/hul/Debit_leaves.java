package com.maven.hul;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Debit_leaves {

	public static void main(String[] args) throws InterruptedException {
		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#");
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		try 
		{
	    //Enter Login details 
		driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("opsadmin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
		Thread.sleep(3000);
		driver.findElement(By.linkText("SIGN IN")).click();
		Thread.sleep(4000);
		//Click on Capacity tab
		driver.findElement(By.xpath("//span[text()='Capacity']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(2000);
		//Click on Leave management tab
		driver.findElement(By.linkText("LEAVE MONITORING")).click();
		Thread.sleep(2000);
		//Enter Employee Number
		driver.findElement(By.xpath("(//input[@name='name'])[2]")).sendKeys("600019073");
		Thread.sleep(2000);
		//Search Employee
		driver.findElement(By.linkText("Search")).click();
		Thread.sleep(2000);
		WebElement element1=driver.findElement(By.xpath("//label[text()='Leave Balance:']/following-sibling::div/div/span[2]"));
		String value= element1.getText();		
		System.out.println("Total Leaves balance before debit " +value );
		
		driver.findElement(By.xpath("//span[text()='Add Transaction']")).click();
		Thread.sleep(2000);
		//Select Type Of Leave
		WebElement LeaveType= driver.findElement(By.xpath("//span[text()='Type Of Leave:']/parent::label/following-sibling::div/div/div[2]"));
		LeaveType.click();
		Thread.sleep(1000);
		WebElement Leave = driver.findElement(By.xpath("//span[text()='Type Of Leave:']/parent::label/following-sibling::div/div/div/input"));
		Leave.sendKeys(Keys.TAB);
		Thread.sleep(3000);
		
		//Select Type Of Transaction
		WebElement TransactionType= driver.findElement(By.xpath("//span[text()='Type Of Transaction:']/parent::label/following-sibling::div/div/div[2]"));
		TransactionType.click();
		Thread.sleep(1000);
		WebElement Debit = driver.findElement(By.xpath("//span[text()='Type Of Transaction:']/parent::label/following-sibling::div/div/div/input"));
	Debit.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(1000);
		Debit.sendKeys(Keys.TAB);
		Thread.sleep(1000);
		
		

		
		//Enter Leave value
		driver.findElement(By.xpath("//input[@name='value']")).sendKeys("2");
		Thread.sleep(1000);
		//Enter Remark
		driver.findElement(By.xpath("//textarea[@name='remark']")).sendKeys("Out off station due to some personal work");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[text()='Add']")).click();
		Thread.sleep(2000);
		WebElement element=driver.findElement(By.xpath("//label[text()='Leave Balance:']/following-sibling::div/div/span[2]"));
		String value1= element.getText();
		
		System.out.println("Successfully debited One Leave,Total Leaves balance after debit " +value1 );
		
		
		driver.quit();
				
		}
		catch(Exception e)
		{
		e.printStackTrace();
		driver.quit();
		}

	}

}
