package com.maven.hul;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AMS_AddDistri_3P {

	public static void main(String[] args) throws InterruptedException {
		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#");
		
		Thread.sleep(3000);
	
	    //Enter Login details 
		driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("opsadmin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
				
				//Click on Sign-up button
				driver.findElement(By.linkText("SIGN IN")).click();
				Thread.sleep(7000);
				
				//Click on Team
				driver.findElement(By.xpath("(//span[text()='Team'])[1]/parent::span/parent::span/parent::a")).click();
				Thread.sleep(2000);
				
				//Click on add dis
				driver.findElement(By.xpath("//span[text()='Add Distributor']/parent::span/parent::span/parent::a")).click();
				Thread.sleep(2000);
				
				//Add distributor
				//Enter Distributor code
				driver.findElement(By.xpath("//input[@name='code']")).sendKeys("ASMDIS002");
				Thread.sleep(2000);
				//Enter Distributor name
				driver.findElement(By.xpath("//input[@name='name']")).sendKeys("Distname first");
				Thread.sleep(2000);
				
				//Select City
				WebElement City=driver.findElement(By.xpath("//input[@name='city']"));
				City.sendKeys("Pune");
				Thread.sleep(2000);
				City.sendKeys(Keys.TAB);
				
				//Select Date of joining
				driver.findElement(By.xpath("(//span[text()='Date Of Joining:'])[2]/parent::label/following-sibling::div/div[1]/div[2]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//span[text()='Today']")).click();
				Thread.sleep(2000);
				
				//Click on stuffing type
				driver.findElement(By.xpath("//span[text()='Staffing Type:']/parent::label/following-sibling::div/div[1]/div[2]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//li[text()='3P']")).click();
				Thread.sleep(1000);
				
				//Select Disti type
				driver.findElement(By.xpath("//span[text()='Distributor Type:']/parent::label/following-sibling::div/div[1]/div[2]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//li[text()='GTM']")).click();
				Thread.sleep(2000);
				WebElement manager=driver.findElement(By.xpath("//input[@name='parents']"));
				manager.sendKeys("333043");
				Thread.sleep(2000);
				manager.sendKeys(Keys.TAB);
				Thread.sleep(1000);
				//
				driver.findElement(By.xpath("//span[text()='Staffing Provider:']/parent::label/following-sibling::div/div/div[2]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//li[text()='Genius']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//input[@name='countMESM']")).sendKeys("5");
				driver.findElement(By.xpath("//input[@name='allocatedBudgetMESM']")).sendKeys("100000");
				driver.findElement(By.xpath("//input[@name='countTLOM']")).sendKeys("4");
				driver.findElement(By.xpath("//input[@name='allocatedBudgetTLOM']")).sendKeys("40000");
				driver.findElement(By.xpath("//input[@name='countFLOAT']")).sendKeys("3");
				driver.findElement(By.xpath("//input[@name='allocatedBudgetFLOAT']")).sendKeys("30000");
				Thread.sleep(3000);
				driver.findElement(By.xpath("//span[text()='Save']/parent::span/parent::span/parent::a")).click();
				Thread.sleep(8000);
				
				driver.quit();
				
	}				

}
