package com.maven.hul;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Add_New_Distributor_3P 
{
	static boolean is_toast_display = false;
	public static void main(String[] args) throws InterruptedException 
	{
		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#");
		Thread.sleep(5000);
	
	    //Enter Login details 
		driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("opsadmin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
				
				//Click on Sign-up button
				driver.findElement(By.linkText("SIGN IN")).click();
				Thread.sleep(7000);
				driver.findElement(By.xpath("//span[text()='Capacity']/parent::span")).click();
				Thread.sleep(3000);
				
				
				for(int i=3;i<4;i++)
				{
				//Click on Action button
				WebElement element = driver.findElement(By.xpath("//*[contains(text(),'Actions')]/following::span[contains(@id,'splitbutton')]"));
				Actions actions = new Actions(driver);
				actions.moveToElement(element).click().perform();
				Thread.sleep(1000);
				//Add Distributor
				driver.findElement(By.xpath("//span[text()='Add Distributor']/parent::a")).click();
				Thread.sleep(1000);
				
				//Add distributor
				//Enter Distributor code
				driver.findElement(By.xpath("//input[@name='code']")).sendKeys("Dist120"+i);
				Thread.sleep(1000);
				//Enter Distributor name
				driver.findElement(By.xpath("(//input[@name='name'])[2]")).sendKeys("Distname first");
				Thread.sleep(1000);
				
				//Select City
				WebElement City=driver.findElement(By.xpath("(//input[@name='city'])[2]"));
				City.sendKeys("Pune");	
				Thread.sleep(2000);
				City.sendKeys(Keys.TAB);
				
				//
				driver.findElement(By.xpath("//span[text()='Date Of Joining:']/parent::label/following-sibling::div/div/div[2]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//span[text()='Today']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//span[text()='Staffing Type:']/parent::label/following-sibling::div/div[1]/div[2]")).click();
				Thread.sleep(2000);
				
				driver.findElement(By.xpath("//li[text()='3P']")).click();
				Thread.sleep(1000);
				
				driver.findElement(By.xpath("//span[text()='Distributor Type:']/parent::label/following-sibling::div/div[1]/div[2]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//li[text()='GTM']")).click();
				Thread.sleep(2000);
				WebElement manager=driver.findElement(By.xpath("//input[@name='parents']"));
				manager.sendKeys("333043");
				Thread.sleep(2000);
				manager.sendKeys(Keys.TAB);
				Thread.sleep(1000);
				
				WebElement weeklyoff = driver.findElement(By.xpath("//span[text()='Weekly Off:']/parent::label/following-sibling::div/div/div/following-sibling::div"));
				weeklyoff.sendKeys("Saunday");
				manager.sendKeys(Keys.TAB);
				Thread.sleep(1000);
				
				Thread.sleep(3000);
				//
				driver.findElement(By.xpath("//span[text()='Staffing Provider:']/parent::label/following-sibling::div/div/div[2]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//li[text()='Genius']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//input[@name='countMESM']")).sendKeys("5");
				driver.findElement(By.xpath("//input[@name='allocatedBudgetMESM']")).sendKeys("100000");
				driver.findElement(By.xpath("//input[@name='countTLOM']")).sendKeys("4");
				driver.findElement(By.xpath("//input[@name='allocatedBudgetTLOM']")).sendKeys("40000");
				driver.findElement(By.xpath("//input[@name='countFLOAT']")).sendKeys("3");
				driver.findElement(By.xpath("//input[@name='allocatedBudgetFLOAT']")).sendKeys("30000");
				Thread.sleep(3000);
				driver.findElement(By.xpath("//span[text()='Save']/parent::span/parent::span/parent::a")).click();
				Thread.sleep(4000);
				
				is_toast_display = driver.findElement(By.xpath("//label[text()='Created customer successfully']")).isDisplayed();
				if(is_toast_display)
				{
					System.out.println("Ditributor added successfully");
				}
				else
				{
					System.out.println("Distributor not created");
				}
				
				}
				driver.quit();
		}

}
