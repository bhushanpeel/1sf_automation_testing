package com.maven.hul;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ME_Exit_Opsadmin {

	public static void main(String[] args) throws InterruptedException {
		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#");
		WebDriverWait wait = new WebDriverWait(driver, 10);
		//Thread.sleep(5000);
	
	    //Enter Login details 
	driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("opsadmin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
		Thread.sleep(1000);
		driver.findElement(By.linkText("SIGN IN")).click();
		Thread.sleep(3000);

		//Click on Capacity tab.
		driver.findElement(By.xpath("//span[text()='Capacity']/parent::span")).click();
		Thread.sleep(4000);
		
		//Search active Status ME
		driver.findElement(By.xpath("//input[@name='status']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//li[text()='Vacant']")).click();
	//	driver.findElement(By.xpath("//li[text()='Pending Review']")).click();
		
		//Search Designation
		WebElement Desi=driver.findElement(By.xpath("//input[@name='manager']"));
		Desi.sendKeys("411574");
		Thread.sleep(2000);
		Desi.sendKeys(Keys.TAB);
	
		//Click on search button
		driver.findElement(By.xpath("//span[text()='Search']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(2000);
		
		
		for (int i=0;i<50;i++)
		{
		//Click on action button
			//driver.findElements(By.xpath("(//span[text()='Actions']/parent::span/parent::span/parent::a)[2])"))
		WebElement element = driver.findElement(By.xpath("(//span[text()='Actions']/parent::span/parent::span/parent::a)[2]"));
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().build().perform();
		Thread.sleep(2000);
		
		//Click on exit button
		driver.findElement(By.xpath("//span[text()='Delete Position']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("(//span[text()='Yes'])[1]/parent::span/parent::span/parent::a")).click();
		Thread.sleep(2000);
		}
		/*
		 * driver.findElement(By.
		 * xpath("//span[text()='Actual Exit Date:']/parent::label/following-sibling::div/div/div[2]"
		 * )).click(); Thread.sleep(2000); driver.findElement(By.xpath(
		 * "//span[text()='Today']/parent::span/parent::span/parent::a")).click();
		 * Thread.sleep(2000); driver.findElement(By.
		 * xpath("//span[text()='Reason for Exit:']/parent::label/following-sibling::div/div/div[2]"
		 * )).click(); Thread.sleep(1000);
		 * driver.findElement(By.xpath("//li[text()='Retirement']")).click();
		 * Thread.sleep(1000); driver.findElement(By.xpath(
		 * "//span[text()='Exit']/parent::span/parent::span/parent::a")).click();
		 * Thread.sleep(1000); driver.findElement(By.xpath(
		 * "(//span[text()='Yes'])[1]/parent::span/parent::span/parent::a")).click();
		 * Thread.sleep(5000);
		 * 
		 * //Click on clear button
		 * driver.findElement(By.xpath("(//span[text()='Clear'])[1]")).click();
		 * Thread.sleep(2000); //Search active Status ME
		 * driver.findElement(By.xpath("//input[@name='status']")).click();
		 * Thread.sleep(1000);
		 * driver.findElement(By.xpath("//li[text()='Exited']")).click();
		 * Thread.sleep(1000); //Click on search button driver.findElement(By.xpath(
		 * "//span[text()='Search']/parent::span/parent::span/parent::a")).click();
		 * Thread.sleep(2000);
		 * 
		 */
		//driver.quit();
		
		
	}

}
