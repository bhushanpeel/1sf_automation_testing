package com.maven.hul;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class attendance_monitoring {

	public static void main(String[] args) throws InterruptedException {
		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		try 
		{
			//Enter Login details 
			driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
			driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("opsadmin");
			driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
			Thread.sleep(5000);
			driver.findElement(By.linkText("SIGN IN")).click();
			Thread.sleep(5000);
								
			//Click on Capacity tab
			driver.findElement(By.xpath("//span[text()='Capacity']/parent::span")).click();
			Thread.sleep(2000);
		    
			//Click on attendance monitoring
			driver.findElement(By.xpath("//span[text()='ATTENDANCE MONITORING']")).click();
			Thread.sleep(2000);
			
			//Enter Employee Number
			driver.findElement(By.xpath("(//input[@name='name'])[2]")).sendKeys("600019073");
			Thread.sleep(1000);
			
			//Click on search button
			driver.findElement(By.xpath("(//span[text()='Search'])[2]/parent::span/parent::span/parent::a")).click();
			Thread.sleep(5000);
			
			//click on todays update button
						
			WebElement element = driver.findElement(By.xpath("(//span[text()='Update'])[1]"));
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().perform();
			element.click();
			Thread.sleep(2000);
				
			
			//Click on present 
			driver.findElement(By.xpath("//div[text()='Present']/parent::div")).click();
			Thread.sleep(2000);
			
			//click on reason
			driver.findElement(By.xpath("//input[@placeholder='Select reason']/parent::div/following-sibling::div")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//li[text()='Other']")).click();
			Thread.sleep(2000);
			
			//Select Reason to update
			driver.findElement(By.xpath("//textarea[@placeholder='Enter reason for update']")).sendKeys("Present but attendance not reflected");
			Thread.sleep(3000);
			
			//Click on Update button
			driver.findElement(By.xpath("(//span[text()='Update']/parent::span/parent::span/parent::a)[10]")).click();
			Thread.sleep(5000);
			
			
			//click on todays update button
			//driver.findElement(By.xpath("(//span[text()='Update'])[1]")).click();
			//Thread.sleep(1000);
			driver.findElement(By.xpath("(//span[text()='Update'])[1]")).click();
			Thread.sleep(5000);
			
			//Click on present 
			driver.findElement(By.xpath("//div[text()='Leave without pay']/parent::div")).click();
			Thread.sleep(2000);
			
			//click on reason
			driver.findElement(By.xpath("//input[@placeholder='Select reason']/parent::div/following-sibling::div")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//li[text()='No Leave Balance']")).click();
			Thread.sleep(2000);
			
			//Select Reason to update
			driver.findElement(By.xpath("//textarea[@placeholder='Enter reason for update']")).sendKeys("Leave without pay ");
			Thread.sleep(5000);
			
			//Click on Update button
			driver.findElement(By.xpath("(//span[text()='Update']/parent::span/parent::span/parent::a)[10]")).click();
			Thread.sleep(3000);
			
			  
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		driver.quit();
	}

	}

