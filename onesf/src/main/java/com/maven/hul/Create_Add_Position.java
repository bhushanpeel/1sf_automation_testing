package com.maven.hul;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Create_Add_Position {

	public static void main(String[] args) throws InterruptedException {
		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#");
		Thread.sleep(3000);
	
	    //Enter Login details 
		driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("opsadmin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
		Thread.sleep(3000);
		
		try {
				//Click on Signup button
				driver.findElement(By.linkText("SIGN IN")).click();
				Thread.sleep(7000);
				driver.findElement(By.xpath("//span[text()='Capacity']/parent::span")).click();
				Thread.sleep(4000);
								
				//Click on Action button
				WebElement element = driver.findElement(By.xpath("//*[contains(text(),'Actions')]/following::span[contains(@id,'splitbutton')]"));
				Actions actions = new Actions(driver);
				actions.moveToElement(element).click().perform();
				Thread.sleep(1000);
				
				//Select Add position button
				driver.findElement(By.xpath("//span[text()='Add Position']/parent::a")).click();
				Thread.sleep(1000);
				
				//Select manager
				WebElement Manager= driver.findElement(By.xpath("(//input[@name='manager'])[2]"));
				Manager.sendKeys("333043");
				Thread.sleep(3000);
				Manager.sendKeys(Keys.TAB);
				Thread.sleep(2000);
				
				//Select designation as ME(Marketing Executive)
				WebElement designation=driver.findElement(By.xpath("(//input[@name='designation'])[2]"));
				designation.sendKeys("Marke");
				Thread.sleep(2000);
				designation.sendKeys(Keys.TAB);
		         
				//Select department
				WebElement depatr= driver.findElement(By.xpath("(//input[@name='department'])[2]"));
				depatr.sendKeys("Dets");
				depatr.sendKeys(Keys.TAB);
				Thread.sleep(2000);
				
				//Select Grade for ME
				driver.findElement(By.xpath("//input[@name='grade']/parent::div/following-sibling::div")).click();
				Thread.sleep(3000);
				
				driver.findElement(By.xpath("//input[@name=\"grade\"]")).click();
				Thread.sleep(2000);
				
		//		WebElement Grade= driver.findElement(By.xpath("//li[text()='A [ 8000 - 10000 ]']"));
			//	depatr.sendKeys("A [ 8000 - 10000 ]");
			//	Grade.sendKeys(Keys.TAB);
				
				Thread.sleep(3000);
				
				//Click on Create position button
				driver.findElement(By.xpath("//span[text()='Create Position']/parent::span/parent::span/parent::a")).click();
				Thread.sleep(3000);
				System.out.println("Marketing Executive position is created successfully ");
				}
	catch(Exception e)
		{
			e.printStackTrace();
		}
	//	driver.quit();
	}

}
