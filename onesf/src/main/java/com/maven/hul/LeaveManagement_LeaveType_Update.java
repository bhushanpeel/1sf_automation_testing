package com.maven.hul;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LeaveManagement_LeaveType_Update {

	public static void main(String[] args) throws InterruptedException {

		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#");
		Thread.sleep(7000);
	
	    //Enter Login details 
		driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("opsadmin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
		Thread.sleep(1000);
		driver.findElement(By.linkText("SIGN IN")).click();
		Thread.sleep(3000);

		//Click on Capacity tab
		driver.findElement(By.xpath("//span[text()='Settings']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//span[text()='LEAVE MANAGEMENT']")).click();
		Thread.sleep(3000);
					
		driver.findElement(By.xpath("//span[text()='LEAVE TYPE']")).click();
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//div[text()=\"Leave 1\"]/parent::td/following-sibling::td[6]/div/div/div/div/a")).click();
        driver.findElement(By.xpath("//span[text()=\"Update\"]")).click();
	}

}

