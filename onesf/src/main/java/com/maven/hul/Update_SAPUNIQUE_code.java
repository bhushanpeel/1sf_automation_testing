 package com.maven.hul;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Update_SAPUNIQUE_code {

	public static void main(String[] args) throws InterruptedException {
		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#");
		Thread.sleep(3000);
		try 
		{
	    //Enter Login details 
		driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("opsadmin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
		Thread.sleep(3000);
		driver.findElement(By.linkText("SIGN IN")).click();
		Thread.sleep(4000);
		//Click on Capacity tab
		driver.findElement(By.xpath("//span[text()='Capacity']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(3000);
		//Select Vacant position
		driver.findElement(By.xpath("//input[@name='status']/parent::div/following-sibling::div")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//li[text()='Active']")).click();
		Thread.sleep(2000);
		//Select Designation
		driver.findElement(By.xpath("//input[@name='designation']")).sendKeys("Dis");
		Thread.sleep(2000);
		WebElement designation = driver.findElement(By.xpath("//input[@name='designation']"));
		designation.sendKeys(Keys.TAB);
		//click on search button
		driver.findElement(By.xpath("//span[text()='Search']")).click();
		Thread.sleep(2000);
		//Click on edit button 
		WebElement element = driver.findElement(By.xpath("(//span[text()='Actions']/parent::span/parent::span/parent::a)[2]"));
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().perform();
		element.click();
		Thread.sleep(2000);
		
		//Click on employee edit button 
		driver.findElement(By.xpath("(//span[text()='Edit Details'])[2]/parent::a/parent::div")).click();
		Thread.sleep(3000);
		//Click on position details button
		driver.findElement(By.xpath("//span[text()='Position Details']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(2000);
		//Enter SAP/Unified code
		driver.findElement(By.xpath("//input[@name='externalPositionRef']")).clear();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@name='externalPositionRef']")).sendKeys("UC001");
		Thread.sleep(2000);
		//Click on update button
		driver.findElement(By.xpath("(//span[text()='Update'])[2]/parent::span/parent::span/parent::a")).click();
		Thread.sleep(3000);
		driver.quit();
		
		
				
		}
		catch(Exception e)
		{
		e.printStackTrace();
		driver.quit();
		}
	}
}
