package com.maven.hul;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Add_Area {

	public static void main(String[] args) throws InterruptedException {
		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#");
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	
	
	    //Enter Login details 
		driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("opsadmin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
		Thread.sleep(3000);
		driver.findElement(By.linkText("SIGN IN")).click();
		Thread.sleep(4000);
		//Click on Capacity tab
		driver.findElement(By.xpath("//span[text()='Settings']/parent::span")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[text()=\"Area Configuration\"]")).click();
		Thread.sleep(2000);
		//Declaration
		String Area = "Testarea_8";
		
		driver.findElement(By.xpath("//span[text()=\"Add New Area\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(" (//input[@name='area'])[2]")).sendKeys(Area);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[text()='Save']")).click();
		Thread.sleep(4000);
						
		if(driver.getPageSource().contains("Area Name already Exist"))
		{
			Thread.sleep(2000);
			driver.findElement(By.xpath("//span[text()='OK']")).click();
			Thread.sleep(2000);
			System.out.println("Area Name already Exist");
			driver.navigate().refresh();
			Thread.sleep(4000);
			/*
			 * for (;;) {
			 * 
			 * }
			 */
			driver.quit();
			System.out.println("Hii");
		    
		}else {
			//Click on Capacity tab
			driver.findElement(By.xpath("//span[text()='Settings']/parent::span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//span[text()=\"Area Configuration\"]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//span[text()=\"Add New Area\"]")).click();
			Thread.sleep(2000);
			
			//Add new Area
			driver.findElement(By.xpath(" (//input[@name='area'])[2]")).sendKeys(Area);
			driver.findElement(By.xpath("//span[text()='Save']")).click();
			Thread.sleep(4000);
			System.out.println("Area added successsfully ");
			driver.quit();
			System.out.println("Hello");

		}
		System.out.println("Exit");
		driver.quit();
		}
	}
	
