package com.maven.hul;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Add_holidaysDistri_login {

	public static void main(String[] args) throws InterruptedException 
	{
		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#");
		Thread.sleep(5000);
		
		try {
		
	    //Enter Login details 
		driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("411574");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
		Thread.sleep(3000);
		driver.findElement(By.linkText("SIGN IN")).click();
		Thread.sleep(5000);
		
		//Click on Team Button
		driver.findElement(By.xpath("(//span[text()='Team'])[1]/parent::span/parent::span/parent::a")).click();
		Thread.sleep(2000);
	
		//Click on Leave section 
		driver.findElement(By.xpath("//span[text()='Leave']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(2000);
		
		//Click on Configuration button
		driver.findElement(By.xpath("(//input[@value='Pending Requests'])[2]/parent::div/parent::div/parent::div/parent::div/following-sibling::a[3]")).click();
		Thread.sleep(2000);
		
		//Click on add holidays button
		driver.findElement(By.xpath("//span[text()='Add Holiday']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(2000);
		
		//Click on Calender button
		driver.findElement(By.xpath("//span[text()='Date:']/parent::label/following-sibling::div/div/div[2]")).click();
		Thread.sleep(2000);

		//Select today date
		driver.findElement(By.xpath("//span[text()='Today']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(2000);
		
		//Enter Holiday destibtion
		driver.findElement(By.xpath("//input[@name='name']")).sendKeys("HOLI");
		Thread.sleep(2000);
		
		//Click on Save button
		driver.findElement(By.xpath("//span[text()='Save']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(2000);
		//Click on ADD Submit button
		driver.findElement(By.xpath("//span[text()='Add']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(2000);
		
		}		
			
			catch(Exception e)
			{
			e.printStackTrace();
			driver.findElement(By.xpath("//span[text()='OK']/parent::span/parent::span/parent::a")).click();
			Thread.sleep(2000);
			
			}
	driver.quit();	
	}
	
	}

