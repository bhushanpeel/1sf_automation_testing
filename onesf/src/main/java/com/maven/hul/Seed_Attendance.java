package com.maven.hul;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Seed_Attendance {

	public static void main(String[] args) throws InterruptedException {
		System.getProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://1sfuat.peel-works.com/OneSFNext/#makesense");
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		System.out.println("Navigate to 1sf portal");
	
	    //Enter Login details 
		driver.findElement(By.xpath("//input[@name='tenantId']")).sendKeys("devqa");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("opsadmin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("password");
		Thread.sleep(5000);
		System.out.println("Enter details success");
		
		//Click on Signup button
		driver.findElement(By.linkText("SIGN IN")).click();
		Thread.sleep(7000);
		System.out.println("login done");
		//Click on Capacity button
		driver.findElement(By.xpath("//span[text()='Capacity']/parent::span")).click();
		Thread.sleep(4000);
		System.out.println("Click on Capacity");
		
	    //Click on seed attendance button
		WebElement element = driver.findElement(By.xpath("//*[contains(text(),'Build Hierarchy')]/parent::span/parent::span/following-sibling::span"));
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().perform();
		Thread.sleep(2000);
		
		//Click on seed attendance option
		driver.findElement(By.xpath("//span[text()='Seed Attendance']")).click();
	    Thread.sleep(2000);
	    System.out.println("Click on Seed attendence");
	    //Click on Seed Attendance button
		driver.findElement(By.xpath("//span[text()='Seed Attendance']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(1000);
		
		//Click on Confirmation Yes button
		driver.findElement(By.xpath("//span[text()='Yes']/parent::span/parent::span/parent::a")).click();
		Thread.sleep(10000);
		System.out.println("Seed Done");
		driver.quit();
		
	}
}