package com.SF.Test;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Test_1_Getreport {
      
	    @Test
	    void getauthentication()
	    {
	    	//Specify base URL
	    	RestAssured.baseURI="https://www.guru99.com";
	    	
	    	//Specify Request object
	    	RequestSpecification httprequest=RestAssured.given();
	    	
	    	//Response Object
	    	Response response = httprequest.request(Method.GET,"software-testing-introduction-importance.html");
	    	
	    	//Print response
	    	String responsebody =response.getBody().asString();
	    	System.out.println("Response body is:"+responsebody);
	    	int statuscode=response.getStatusCode();
	    	System.out.println("Status code is :"+statuscode);
	    	Assert.assertEquals(statuscode, 200);
	    	
	    }

}
